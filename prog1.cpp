#include <iostream>
#include <vector>
#include <random>
#include <chrono>
#include <fstream>



void fillrandom(std::vector<int> &a, int size, int max_value);
void fillascending(std::vector<int> &a, int size, int x);
void printArr(std::vector<int> a, std::ofstream &file);
int Partition(std::vector<int> &a,int p,int r);
int RandomPartition(std::vector<int> &a,int p,int r);
void QuickSort(std::vector<int> &a,int p,int r);
void RandomQuickSort(std::vector<int> &a,int p,int r);
void Swap(std::vector<int> &a,int x,int y);


int main(){
    int size;
    int X;

    std::cout<<"Enter the value of N :";
    std::cin>>size;
    std::cout<<"Enter the value of X :";
    std::cin>>X;

    std::vector<int> arr1;
    std::vector<int> arr2;

    std::ofstream myfile ("output.txt");//it will create the file for you.
    if (myfile.is_open()){
        std::chrono::time_point<std::chrono::system_clock> start, end;//timer.

        // Quick sort-Ascending
        fillascending(arr1,size,X);
        myfile << "*** Quick sort-Ascending distribution***\n";
        printArr(arr1,myfile);
        start = std::chrono::system_clock::now();//Timer start.
        QuickSort(arr1,0,size-1);
        end = std::chrono::system_clock::now();//Timer end.
        std::chrono::duration<double> elapsed_seconds = end - start;
        printArr(arr1,myfile);
        myfile << "It took " << elapsed_seconds.count()*1000<< " milliseconds \n";
        arr1.clear();

        // Quick sort-random
        fillascending(arr1,size,X);
        myfile << "*** Random Quick sort-Ascending distribution ***\n";
        printArr(arr1,myfile);
        start = std::chrono::system_clock::now();
        RandomQuickSort(arr1,0,size-1);
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds2 = end - start;
        printArr(arr1,myfile);
        myfile << "It took " << elapsed_seconds2.count()*1000 << " milliseconds \n";
        arr1.clear();

        // random Quick sort-Ascending
        fillrandom(arr2,size,1000);
        myfile << "***Quick sort-Random distribution***\n";
        printArr(arr2,myfile);
        start = std::chrono::system_clock::now();
        QuickSort(arr2,0,size-1);
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds3 = end - start;
        printArr(arr2,myfile);
        myfile << "It took " << elapsed_seconds3.count()*1000 << " milliseconds \n";
        arr2.clear();

        // random Quick sort-random
        fillrandom(arr2,size,1000);
        myfile << "*** Random Quick sort-Random distribution***\n";
        printArr(arr2,myfile);
        start = std::chrono::system_clock::now();
        RandomQuickSort(arr2,0,size-1);
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds4 = end - start;
        printArr(arr2,myfile);
        myfile << "It took " << elapsed_seconds4.count()*1000 << " milliseconds\n";
        arr2.clear();

        myfile.close();
    }else{
        std::cout << "Unable to open file";
    }

     return(0);
}

void fillrandom(std::vector<int> &a, int size, int max_value){
    a.reserve(size);

    std::default_random_engine generator;//Random number generator
    std::uniform_int_distribution<unsigned int> distribution(1, max_value);//Uniform distribution.

    for (int i = 0; i < size; i++) {
        a.push_back(distribution(generator));
    }
}

void fillascending(std::vector<int> &a, int size, int x){
    a.reserve(size);
    for (int i = 0; i < size; i++) {
        a.push_back(size + (i*x));
    }
}
void printArr(std::vector<int> a,std::ofstream &file)
{
    for (auto i : a)
        file << i << "  ";

    file << std::endl;
}
int Partition(std::vector<int> &a,int p,int r){
    int x = a[r];
    int i = p - 1;
    for(int j = p; j < r;j++){
        if(a[j] <= x ){
            i++;
            Swap(a,i,j);
        }
    }
    Swap(a,i+1,r);
    return(i+1);
}


int RandomPartition(std::vector<int> &a,int p,int r){
    double d = ((double) rand() / (RAND_MAX));
    int index = p + floor((r-p+1) * d);
    Swap(a,r,index);
    return (Partition(a,p,r));
}


void QuickSort(std::vector<int> &a,int p,int r){
    if(p < r){
        int q = Partition(a,p,r);
        QuickSort(a,p,q-1);
        QuickSort(a, q+1, r);
    }
}

void RandomQuickSort(std::vector<int> &a,int p,int r){
    if(p < r){
        int q = RandomPartition(a,p,r);
        RandomQuickSort(a,p,q-1);
        RandomQuickSort(a, q+1, r);
    }
}

void Swap(std::vector<int> &a,int x,int y){
    int temp = a[x];
    a[x] = a[y];
    a[y] = temp;
}
